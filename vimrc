"Standard Settings
syntax on

set encoding=utf-8

filetype plugin on

set nobackup
set nowritebackup

set updatetime=300

set signcolumn=yes

set tabstop=4
set shiftwidth=4
set expandtab

set number
set relativenumber
set autoindent
set showmatch
set cursorline

inoremap jj <ESC>

autocmd BufEnter *.{js,jsx} :syntax sync fromstart
autocmd BufLeave *.{js,jsx} :syntax sync clear

"Plugins
call plug#begin('~/.vim/plugins')

Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'nordtheme/vim'
"Plug 'pangloss/vim-javascript'
"Plug 'styled-components/vim-styled-components', {'branch': 'main'}
"Plug 'morhetz/gruvbox'
Plug 'dense-analysis/ale'
Plug 'kristijanhusak/vim-dadbod-completion'
Plug 'kristijanhusak/vim-dadbod-ui'
Plug 'tpope/vim-dadbod'

call plug#end()

"""Extras
colorscheme nord
"colorscheme gruvbox
"ALE
let g:ale_lint_on_enter=1
let g:ale_lint_on_save=1
let g:ale_linters={
            \ 'javascript': ['eslint'],
            \ }
"let g:ale_completion_enabled=1
nmap <silent> <C-e> <Plug>(ale_next_wrap)
"COC
inoremap <silent><expr> <TAB>
    \ coc#pum#visible() ? coc#pum#next(1) :
    \ CheckBackspace() ? "\<Tab>" :
    \ coc#refresh()
inoremap <expr><S-TAB> coc#pum#visible() ? coc#pum#prev(1) : "\<C-n>"
inoremap <silent><expr> <CR> coc#pum#visible() ? coc#pum#confirm()
    \: "\<C-g>u\<CR>\<c-r>=coc#on_enter()\<CR>"
function! CheckBackspace() abort
    let col = col('.') - 1
    return !col || getline('.')[col - 1] =~# '\s'
endfunction

"""Status line"""
set laststatus=2
set statusline=
set statusline+=\ %{b:gitbranch}
set statusline+=%t
set statusline+=\ %{StatuslineMode()}
set statusline+=\ %m
set statusline+=%=
set statusline+=%{winnr()}
set statusline+=\ %y
set statusline+=\ %P
set statusline+=\ %c:%l
set statusline+=\ %{strftime(\"%H:%M\")}
set statusline+=\ 

function! StatuslineGitBranch()
  let b:gitbranch=""
  if &modifiable
    try
      let l:dir=expand('%:p:h')
      let l:gitrevparse = system("git -C ".l:dir." rev-parse --abbrev-ref HEAD")
      if !v:shell_error
        let b:gitbranch="(".substitute(l:gitrevparse, '\n', '', 'g').") "
      endif
    catch
    endtry
  endif
endfunction

augroup GetGitBranch
  autocmd!
  autocmd VimEnter,WinEnter,BufEnter * call StatuslineGitBranch()
augroup END

function! StatuslineMode()
  let l:mode=mode()
  if l:mode==#"n"
    return "NORMAL"
  elseif l:mode==?"v"
    return "VISUAL"
  elseif l:mode==#"i"
    return "INSERT"
  elseif l:mode==#"R"
    return "REPLACE"
  elseif l:mode==?"s"
    return "SELECT"
  elseif l:mode==#"t"
    return "TERMINAL"
  elseif l:mode==#"c"
    return "COMMAND"
  elseif l:mode==#"!"
    return "SHELL"
  endif
endfunction


# The following lines were added by compinstall
zstyle :compinstall filename '/home/mouse/.zshrc'

autoload -Uz compinit
compinit
# End of lines added by compinstall

### My Config ###
#plugins
source /home/mouse/.zshplugins/zsh-git-prompt/zshrc.sh
source /home/mouse/.zshplugins/zsh-autosuggestions/zsh-autosuggestions.zsh
source /home/mouse/.zshplugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh 

#load colors
autoload colors && colors
for COLOR in RED GREEN YELLOW BLUE MAGENTA CYAN BLACK WHITE; do
    eval $COLOR='%{$fg_no_bold[${(L)COLOR}]%}'  #wrap colours between %{ %} to avoid weird gaps in autocomplete
    eval BOLD_$COLOR='%{$fg_bold[${(L)COLOR}]%}'
done
eval RESET='%{$reset_color%}'

#prompt
PROMPT=$'${CYAN}%n${RESET}::${CYAN}%m${RESET}@${CYAN}%2d${RESET} - ${GREEN}%*\n${RED}%#${RESET} '
RPROMPT='$(git_super_status)'

#path
export PATH="$HOME/bin:$PATH"
export PAGER=most
export EDITOR=nano

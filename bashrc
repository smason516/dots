# BASHRC
# Sydney Mason
# 01/21/2021

test -s ~/.alias && . ~/.alias || true

# GIT Info
function parse_git_dirty {
  [[ $(git status --porcelain 2> /dev/null) ]] && echo "*"
}
function parse_git_branch {
  git branch --no-color 2> /dev/null | sed -e '/^[^*]/d' -e "s/* \(.*\)/ (\1$(parse_git_dirty))/"
}

# START PROMPT
# Prompt color settings
set_green="\[\033[38;5;046m\]"
set_orange="\[\033[38;5;214m\]"
set_cyan="\[\033[38;5;050m\]"
reset="\[\033[0;00m\]"
# Prompt contents
user="${set_green}\u"
colons="${set_orange}::"
host="${set_green}\h"
at="${set_orange}@"
cwd="${set_cyan}"'$(pwd)'
git_branch="${set_orange}\$(parse_git_branch)"
arrows="\n${set_orange}>>>${reset} "
# Prompt
PS1="${user}${colons}${host}${at}${cwd}${git_branch}${arrows}"
# END PROMPT

# Aliases
alias ll="ls -ltrh"

# ZSH-like tab completion
bind 'TAB:menu-complete'

# Exports
export PAGER=most
export EDITOR=nano
export PATH="$HOME/bin:$PATH"

# Sensei TEN sensitivity
#rivalcfg --sensitivity 500

# Start TMUX
if command -v tmux &> /dev/null && [ -z "$TMUX" ]; then
    tmux attach -t mischief || tmux new -s mischief
fi

export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion

vterm_printf() {
    if [ -n "$TMUX" ] && ([ "${TERM%%-*}" = "tmux" ] || [ "${TERM%%-*}" = "screen" ]); then
        # Tell tmux to pass the escape sequences through
        printf "\ePtmux;\e\e]%s\007\e\\" "$1"
    elif [ "${TERM%%-*}" = "screen" ]; then
        # GNU screen (screen, screen-256color, screen-256color-bce)
        printf "\eP\e]%s\007\e\\" "$1"
    else
        printf "\e]%s\e\\" "$1"
    fi
}

if [[ "$INSIDE_EMACS" = 'vterm' ]]; then
    function clear() {
        vterm_printf "51;Evterm-clear-scrollback";
        tput clear;
    }
fi

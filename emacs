;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; File name: ` ~/.emacs '
;;; ---------------------
;;;
;;; If you need your own personal ~/.emacs
;;; please make a copy of this file
;;; an placein your changes and/or extension.
;;;
;;; Copyright (c) 1997-2002 SuSE Gmbh Nuernberg, Germany.
;;;
;;; Author: Werner Fink, <feedback@suse.de> 1997,98,99,2002
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; Test of Emacs derivates
;;; -----------------------
(if (string-match "XEmacs\\|Lucid" emacs-version)
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ;;; XEmacs
  ;;; ------
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    (progn
      (if (file-readable-p "~/.xemacs/init.el")
          (load "~/.xemacs/init.el" nil t))
      )
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ;;; GNU-Emacs
  ;;; ---------
  ;;; load ~/.gnu-emacs or, if not exists /etc/skel/.gnu-emacs
  ;;; For a description and the settings see /etc/skel/.gnu-emacs
  ;;;   ... for your private ~/.gnu-emacs your are on your one.
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  (if (file-readable-p "~/.gnu-emacs")
      (load "~/.gnu-emacs" nil t)
    (if (file-readable-p "/etc/skel/.gnu-emacs")
	(load "/etc/skel/.gnu-emacs" nil t)))

  ;; Custom Settings
  ;; ===============
  ;; To avoid any trouble with the customization system of GNU emacs
  ;; we set the default file ~/.gnu-emacs-custom
  (setq custom-file "~/.gnu-emacs-custom")
  (load "~/.gnu-emacs-custom" t t)
;;;
  )
;;;

;; My Configuration
(setq custom-file (expand-file-name "custom.el" user-emacs-directory))
(when (file-exists-p custom-file)
  (load custom-file))

;; Setup package repos: Elpa, Melpa, Packages(require 'package)
(add-to-list 'package-archives '("melpa"."https://melpa.org/packages/") t)
(add-to-list 'package-archives '("melpa-stable" . "http://stable.melpa.org/packages/"))
(add-to-list 'package-archives '("nongnu" . "https://elpa.nongnu.org/nongnu/"))
(add-to-list 'load-path "~/.emacs.d/personal/")
(setq package-enable-at-startup nil)
(package-initialize)

;; Basic QOL modifications
(setq inhibit-startup-message -1)
(tool-bar-mode -1)
(scroll-bar-mode -1)
(defalias 'yes-or-no 'y-or-n-p)
(tab-bar-mode t)
;;(global-hl-line-mode 1)

;; Sets up relative line numbers globally
(setq display-line-numbers-type 'relative)
(global-display-line-numbers-mode)

;; Tabs/indentation settings
(setq-default indent-tabs-mode nil)
(setq tab-width 4)
(defvaralias 'c-basic-offset 'tab-width)
(setq js-indent-level 4)

;; Sets up 'use-package to handle package management
(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))
(require 'use-package)
(setq use-package-always-ensure 't)

;; Sets theme
(use-package monokai-theme)
(use-package leuven-theme)
(use-package nord-theme)
;;(load-theme 'monokai t)
;;(load-theme 'leuven-dark t)
;;(load-theme 'leuven t)
(load-theme 'nord t)


;; Defines backup directory
(make-directory (expand-file-name "backups/" user-emacs-directory) t)
(setq backup-directory-alist `(("." . ,(expand-file-name "backups/" user-emacs-directory))))

;; Sets `hunspell` to the spellchecking program and makes flyspell togglable
(setq ispell-program-name "hunspell")
(defun flyspell-on-for-buffer-type ()
  "Enable Flyspell appropriately for the major mode of the current buffer."
  "Uses `flyspell-prog-mode' for modes derived from `prog-mode', so only strings and comments get checked."
  "All other buffers get `flyspell-mode' to check all text.  If flyspell is already enabled, does nothing. twump "
  (interactive)
  (if (not (symbol-value flyspell-mode)) ; if not already on
      (progn
	(if (derived-mode-p 'prog-mode)
	    (progn
	      (message "Flyspell on (code)")
	      (flyspell-prog-mode))
	  ;; else
	  (progn
	    (message "Flyspell on (text)")
	    (flyspell-mode 1)))
	;; I tried putting (flyspell-buffer) here but it didn't seem to work
	)))
(defun flyspell-toggle ()
  "Turn Flyspell on if it is off, or off if it is on."
  "When turning on, it uses `flyspell-on-for-buffer-type' so code-vs-text is handled appropriately."
  (interactive)
  (if (symbol-value flyspell-mode)
      (progn ; flyspell is on, turn it off
	(message "Flyspell off")
	(flyspell-mode -1))
    ; else - flyspell is off, turn it on
    (flyspell-on-for-buffer-type)))
(global-set-key (kbd "C-c f") 'flyspell-toggle )

;; Sets up colored delimiters
(use-package rainbow-delimiters
  :init
  (add-hook 'prog-mode-hook 'rainbow-delimiters-mode))

(use-package company-mode
  :init
  (add-hook 'after-init-hook 'global-company-mode)
  (setq company-minimum-prefix-length 1
	company-idle-delay 0.0))

;; Installs YAML mode
(use-package yaml-mode
  :ensure t)

;; Installs vterm
(use-package vterm
  :ensure t)

;; LSP Configuraiton
(use-package lsp-mode
  :init
  ;; set prefix for lsp-command-keymap (few alternatives - "C-l", "C-c l")
  (setq lsp-keymap-prefix "C-c l")
  :hook (;; replace XXX-mode with concrete major-mode(e. g. python-mode)
         (js-mode  . lsp)
         (c-mode   . lsp)
         (c++-mode . lsp)
         (sh-mode  . lsp)
         (json-mode . lsp)
         (markdown-mode . lsp)
         (yaml-mode . lsp)
         ;; if you want which-key integration
         ;;(lsp-mode . lsp-enable-which-key-integration))
         :commands lsp))
(use-package lsp-ui
  :ensure t
  :commands lsp-ui-mode)

;;(use-package dired-sidebar
;;  :ensure t
;;  :commands (dired-sidebar-toggle-sidebar)
;;  :bind (("C-x C-n" . dired-sidebar-toggle-sidebar))
;;  :init
;;  (add-hook 'dired-sidebar-mode-hook
;;	    (lambda ()
;;	      (unless (file-remote-p default-directory)
;;		(auto-revert-mode))))
;;  :config2
;;  (push 'toggle-window-split dired-sidebar-toggle-hidden-commands)
;;  (push 'rotate-windows dired-sidebar-toggle-hidden-commands)

;;  (setq dired-sidebar-subtree-line-prefix "__")
;;  (setq dired-sidebar-theme 'vscode)
;;  (setq dired-sidebar-use-term-integration t)
;;  (setq dired-sidebar-use-custom-font t))

;; Org Mode
(use-package org
  :ensure org-plus-contrib
  :config
  (setq org-agenda-skip-scheduled-if-done t
        org-agenda-skip-deadline-if-done t
        org-agenda-include-deadlines t
        org-agenda-block-separator #x2501
        org-agenda-compact-blocks t
        org-agenda-start-with-log-mode t)
  (with-eval-after-load 'org-journal
    (define-key org-journal-mode-map (kbd "<C-tab>") 'yas-expand))
  (setq org-agenda-clockreport-parameter-plist
        (quote (:link t :maxlevel 5 :fileskip0 t :compact t :narrow 80)))
  (setq org-agenda-deadline-faces
        '((1.0001 . org-warning)
          (0.0    . org-upcoming-deadline)))
  :bind
  (("C-c a" . org-agenda)
   ("C-c c" . org-capture)))

;; Dashboard
(use-package dashboard
  :ensure t
  :config
  (dashboard-setup-startup-hook))

;; Startup Screen
;;(defun emacs-startup-screen ()
;;  "Display the weekly org-agenda and all todos."
;;  (org-agenda nil "n"))
;;(add-hook 'emacs-startup-hook #'emacs-startup-screen)
